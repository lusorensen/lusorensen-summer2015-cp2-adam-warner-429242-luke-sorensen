<?php

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
    
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
	exit;
}

$stmt = $mysqli->prepare("delete from comments WHERE id=?");

if(!$stmt){
    
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
	exit;
}

$stmt->bind_param('s', $id);

$id= htmlentities($_POST["id"]);

$stmt->execute();

$stmt->close();

echo json_encode(array(
		"success" => true,
		
	));
	exit;


?>

