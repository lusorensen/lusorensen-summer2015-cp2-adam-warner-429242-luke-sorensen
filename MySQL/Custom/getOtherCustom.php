<?php

header("Content-Type: application/json"); 

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
    exit;
}

$stmt = $mysqli->prepare("select background, title, bgrColor, hrColor,
                         bgrImage, typeface, ttlColor, txtColor FROM custom WHERE username=?");
if(!$stmt){
    
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't retrieve theme!"
	));
    exit;
}

$username= $_SESSION['other'];

// Bind the parameter
$stmt->bind_param('s', $username);

$stmt->execute();

$result = $stmt->get_result();
 
while($row = $result->fetch_assoc()){
    
	$background = htmlentities($row["background"]);
    $title= htmlentities($row["title"]);
    $bgrColor= htmlentities($row["bgrColor"]);
    $hrColor= htmlentities($row["hrColor"]);
    $bgrImage = htmlentities($row["bgrImage"]);
    $typeface= htmlentities($row["typeface"]);
    $ttlColor= htmlentities($row["ttlColor"]);
    $txtColor= htmlentities($row["txtColor"]);
}

$stmt->close();


//publish as json
echo json_encode(array(
    "success" => true,
    "background" => $background,
    "title"=> $title,
    "bgrColor"=> $bgrColor,
    "hrColor"=> $hrColor,
    "bgrImage"=> $bgrImagemg,
    "typeface"=> $typeface,
    "ttlColor"=> $ttlColor,
    "txtColor"=> $txtColor
		
	));
exit;




    
?>
    