<?php

header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');

if($mysqli->connect_errno) {
    echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
	exit;
}

if(empty($_SESSION['username'])){
    echo json_encode(array(
		"success" => false,
		"message" => "Must be logged in"
	));
	exit;
}


$username = $_SESSION['username'];
$id= $_POST["id"];
$comment= $_POST["comment"];


$stmt = $mysqli->prepare("insert into comments (username, postId, comment) values (?, ?, ?)");

if(!$stmt){
    
echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
	exit;
}   

$stmt->bind_param('sss', $username, $id, $comment);

$stmt->execute();

$stmt->close();

echo json_encode(array(
		"success" => true,
	));
exit;



?>