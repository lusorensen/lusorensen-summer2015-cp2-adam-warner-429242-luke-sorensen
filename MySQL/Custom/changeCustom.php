<?php

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
    exit;
}

$stmt = $mysqli->prepare("update custom set background=?, title=?, bgrColor=?, hrColor=?,
                         bgrImage=?, typeface=?, ttlColor=?, txtColor=? WHERE username=?");

if(!$stmt){
    
	echo json_encode(array(
		"success" => false,
		"message" => "Invalid"
	));
    exit;
}

$background = $_POST["bgr"];
$title= $_POST["ttl"];
$bgrColor= $_POST["bgrColor"];
$hrColor= $_POST["hrColor"];
$bgrImage = $_POST["img"];
$typeface= $_POST["typeface"];
$ttlColor= $_POST["ttlColor"];
$txtColor= $_POST["txtColor"];
$username= $_SESSION["username"];

// Bind the parameter
$stmt->bind_param('sssssssss', $background, $title, $bgrColor, $hrColor, $bgrImage, $typeface, $ttlColor, $txtColor, $username);

$stmt->execute();

$stmt->close();

echo json_encode(array(
		"success" => true,
	));
exit;
    
?>
    