<!DOCTYPE html>
<head>
<title>User Page</title>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="script/UserPage/headerForms.js"></script>
<script type="text/javascript" src="script/UserPage/createPosts.js"></script>
<script type="text/javascript" src="script/UserPage/customizePage.js"></script>
<script type="text/javascript" src="script/UserPage/privacy.js"></script>
<script type="text/javascript" src="script/getComments.js"></script>

<script>
	
$(document).ready(function(){
	renderCustom();
	getFeed();
	
});


function getFeed() {
	$.ajax({url: "MySQL/Feed/myFeed.php", success: function(result){
        $("#feed").html(result);
    }});
}

function logOut() {
	
	$.ajax({url: 'MySQL/Custom/getCustom.php', dataType: 'json', success: function(json) {
	
	if (json.success) {
		
		alert("Bye!!");
		window.location.href = "OpeningPage.php"
		
	}else{
		
		alert("connection error");
	}
}});
}

function mainPage() {
	
	 window.location.href = "MainPage.php";
	 
}

function messagePage() {
	
	 window.location.href = "Messages.php";
	 
}

function privacyFeed() {
	$.ajax({url: "MySQL/Privacy/getPrivacyFeed.php", success: function(result){
         $("#redirect").append(result);
    }});
}
</script>

<style>
    
html, body {
    height: 100%;
}

html {
    display: table;
    margin: auto;
}

body {
    font-family: "Lucida Console", Monaco, monospace;
    display: table-cell;
    vertical-align: middle;
}
	
h1 {
    font-size: 80px;
    font-weight: bold;
	max-width: 600px
}

input[type=button] {
    
	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 20px;
	background: #3498db;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
    
}

input[type=button]:hover {
  background: #ffd4ff;
  text-decoration: none;
  
}

input[type=button]:focus {
	outline:0;
}

input[type=submit] {
    
	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 20px;
	background: #3498db;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
    
}

input[type=submit]:hover {
	
  background: #ffd4ff;
  text-decoration: none;
  
}

input[type=submit]:focus {
	
	outline:0;
}

input[type=text] {
  font-family: "Lucida Console", Monaco, monospace;
}

input[type=file] {
 	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 15px;
	background: #00CC66;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
}

textarea {
	font-family: "Lucida Console", Monaco, monospace;
	line-height: 150%;
	border-width:0px;
	padding: 10px;
}

p {
	max-width: 550px;
	line-height: 150%
	
}

img {
	max-width: 600px;
}
hr { 
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-style: solid;
    border-width: 3px;
	color:#00CC66;
}

button {
    
	border-radius: 5px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #000000;
	font-size: 12px;
	background: #F6F6F6;
	padding: 7px 10px 5px 10px;
	text-decoration: none;
	border-width:0px
    
}

button:hover {
  background: #FF7575;
  text-decoration: none;
  
}

button:focus {
	outline:0;
}

</style>


</head>
<body>
	

	

<h1 align=center id=title> Share Space </h1>

<div id="upload" align="center">

<input type="button" onclick= "imagePostForm()" value="Image">
&nbsp; 
<input type="button" onclick= "textPostForm()" value="Text">
&nbsp; 
<input type="button" onclick= "linkPostForm()" value="Link">
&nbsp; 
<input type="button" onclick= "videoPostForm()" value="Video">
&nbsp; 
<input type="button" onclick= "customizeForm()" value="Customize">
</div>
<br>
<br>


<div id="feed" align="center">

</div>
<br>
<br>
<div id="redirect" align="center">

<input type="button" onclick= "mainPage()" value="Main Page">
&nbsp;
<input type="button" onclick= "privacyForms()" value="Privacy">
&nbsp;
<input type="button" onclick= "messagePage()" value="Messages">
&nbsp;
<input type="button" onclick= "logOut()" value="Log Out">
&nbsp;

</div>
<br>
<br>
        
</body>
</html>