//post text form data to mysql data base
function sendMessage() {
	var receiver = $("#receiver").val();
	var message = $("#messageText").val();
	
	$.ajax({type: 'POST', url: 'Message/sendMessage.php', data: {"receiver": receiver, "message": message},
	dataType: 'json', success: function(json) {
		if (json.success) {
			alert("Message sent!");
			restoreUser();
			getSentMessages();
		}else{	
			alert(json.message);
		}
	}});
}