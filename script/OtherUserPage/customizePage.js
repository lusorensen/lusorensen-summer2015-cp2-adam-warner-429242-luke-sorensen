function renderOtherCustom() {
	
	$.ajax({url: 'MySQL/Custom/getOtherCustom.php', dataType: 'json', success: function(json) {
		
		if (json.success) {
			
			//title
			$("#title").html(json.title);
			
			//background color
			$("body").css("background-color", json.bgrColor);
			
			//typeface
			$("body").css("font-family", json.typeface);
			
			//title color
			$("h1").css("color", json.ttlColor);
			
			//text color
			$("body").css("color", json.txtColor);
			
			//button color
			$("hr").css("color", json.hrColor);
			
		}else{
			
			alert(json.message);
		}
	}});
}