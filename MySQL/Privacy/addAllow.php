<?php
//takes in username and password and adds to user table

header("Content-Type: application/json");
ini_set("session.cookie_httponly", 1);
session_start();


$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit; 
}

$allows = htmlentities($_POST['allows']);

$stmt1 = $mysqli->prepare("SELECT COUNT(*) FROM users WHERE username=?");

// Bind the parameter
$stmt1->bind_param('s', $allows);
$stmt1->execute();

$result = $stmt1->get_result();

while($row = $result->fetch_assoc()){
	$count= htmlentities($row["COUNT(*)"]);	
}

$stmt1->close();

if($count==1){

	$stmt = $mysqli->prepare("insert into privacy (username, allows) values (?, ?)");
    
	if(!$stmt){
		echo json_encode(array(
			"success" => false,
			"message" => "Couldn't connect to database"
		));
		exit;
	}
	$username=$_SESSION['username'];
    
	$stmt->bind_param('ss', $username, $allows);
	 
	$stmt->execute();
	
	$stmt->close();
	 
	echo json_encode(array(
        "success" => true, 
        "message" => "$allows can now access your page!!"
        ));
        exit;
	}

else{
	
	echo json_encode(array(
		"success" => false,
		"message" => "$allows is not a user"
	));
	exit; 
}
?>