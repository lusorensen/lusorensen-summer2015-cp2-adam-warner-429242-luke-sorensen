<?php

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo ("helo");
}

$stmt = $mysqli->prepare("select id, type, title, src, text, posted, username FROM posts where id < ? order by posted desc ");

if(!$stmt){
	echo("error");
}

$stmt->bind_param('s', $limit);

$limit=$_POST["last"];

$stmt->execute();

$result = $stmt->get_result();

$counter=0;

while(($row = $result->fetch_assoc()) && ($counter<4)){
    
	$id = htmlentities($row["id"]);
	$type = htmlentities($row["type"]);
	$title = htmlentities($row["title"]);
    $src= htmlentities($row["src"]);
    $posted= htmlentities($row["posted"]);
    $text= htmlentities($row["text"]);
    $username= htmlentities($row["username"]);
    
    if ($type=='video') {
        
        echo "<br>
        <h3> $title </h3>
        <br>
        <iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/$src\" frameborder=\"0\" allowfullscreen></iframe>
        <br>
        <p>$text</p>
        <br>
        <h6>posted by $username $posted</h6>
        <br>
		<div id=$id align= \"center\">
		<input class=\"comments\" type=\"button\" onclick= \"getComments($id)\" value=\"Comments\">
		<br>
		</div>
		<br>
        <hr>";
             
    }
    
    if ($type=='text') {
        
        echo "<br>
        <h3> $title </h3>
        <br>
        <p> $text </p>
        <br>
        <h6>posted by $username $posted</h6>
        <br>
		<div id=$id align= \"center\">
		<input class=\"comments\" type=\"button\" onclick= \"getComments($id)\" value=\"Comments\">
		<br>
		</div>
		<br>
        <hr>";
        
    }
    
    if ($type=='image'){
        
        echo "<br>
        <h3> $title </h3>
        <br>
        <img src=\"Users/$username/$src\" alt=$src>
        <br>
        <p> $text </p>
        <br>
        <h6> posted by $username $posted</h6>
        <br>
		<div id=$id align= \"center\">
		<input class=\"comments\" type=\"button\" onclick= \"getComments($id)\" value=\"Comments\">
		<br>
		</div>
		<br>
        <hr>";
        
    }
    
    if ($type=='link'){
        
        echo "<br>
        <h3> <a href=\"$src\" target=\"_blank\">$title</a> </h3>
        <br>
        <p> $text </p>
        <br>
        <h6>posted by $username $posted</h6>
        <br>
		<div id=$id align=\"center\">
		<input class=\"comments\" type=\"button\" onclick= \"getComments($id)\" value=\"Comments\">
		<br>
		</div>
		<br>
        <hr>";
        
    }
    
    $counter++;
    
    if(($counter==4) && ($id!=1)){
        
        echo "<div id=\"more\">
		<br>
		<br>
        <input type=\"button\" value=\"More Posts\" id=\"next10\" onclick=\"getMore($id)\">
		</div>";
        
    }
	
	if($id==1){
		echo "<br>
		<p> No more posts! </p>
		<br>";
	}
    
}

$stmt->close();

?>

