<!DOCTYPE html>
<head>
<title>User Page</title>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="Message/messageForm.js"></script>
<script type="text/javascript" src="Message/createMessage.js"></script>

<script>
	

function getInboxList() {
	$.ajax({url: "Message/myMessages.php", success: function(result){
        $("#feed").html(result);
    }});
}


function getSentMessages() {
	$.ajax({url: "Message/sentMessages.php", success: function(result){
        $("#feed").html(result);
    }});
}

function userPage() {
    
    window.location.href = "UserPage.php"
    
}

</script>

<style>
    
html, body {
    height: 100%;
}

html {
    display: table;
    margin: auto;
}

body {
    font-family: "Lucida Console", Monaco, monospace;
    display: table-cell;
    vertical-align: middle;
    background-color: #B4C9ED;
}
	
h1 {
    font-size: 80px;
    font-weight: bold;
	max-width: 600px
}

input[type=button] {
    
	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 20px;
	background: #3498db;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
    
}

input[type=button]:hover {
  background: #ffd4ff;
  text-decoration: none;
  
}

input[type=button]:focus {
	outline:0;
}

input[type=submit] {
    
	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 20px;
	background: #3498db;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
    
}

input[type=submit]:hover {
	
  background: #ffd4ff;
  text-decoration: none;
  
}

input[type=submit]:focus {
	
	outline:0;
}

input[type=text] {
  font-family: "Lucida Console", Monaco, monospace;
}

input[type=file] {
 	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 15px;
	background: #00CC66;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
}

textarea {
	font-family: "Lucida Console", Monaco, monospace;
	line-height: 150%;
	border-width:0px;
	padding: 10px;
}

p {
	max-width: 550px;
	line-height: 150%
	
}

img {
	max-width: 600px;
}
hr { 
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-style: solid;
    border-width: 3px;
	color:#00CC66;
}

</style>
</head>

<body>

<h1 align=center id=title> Messages </h1>

<div id="message" align="center">
<input type="button" onclick= "newMessageForm()" value="Compose new Message">
<br>
<br>
</div>
<br>

<div align="center">
<input type="button" onclick="getInboxList()" value="Inbox">

<input type="button" onclick="getSentMessages()" value="Sent">

<input type="button" onclick="userPage()" value= "User Page">


</div>

<br>
<br>


<div id="feed" align="center">

</div>
<br>
<br>

<br>
<br>
        
</body>
</html>
