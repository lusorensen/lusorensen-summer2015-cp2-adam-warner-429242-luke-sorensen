<!DOCTYPE html>
<head>
<title>Other Page</title>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="script/OtherUserPage/customizePage.js"></script>
<script type="text/javascript" src="script/OtherUserPage/headerForms.js"></script>
<script type="text/javascript" src="script/getComments.js"></script>

<script>
	
$(document).ready(function(){

	
	$.ajax({type: 'POST', url: "MySQL/Privacy/getPrivacy.php", dataType: 'json', success: function(json) {
		if (json.success) {

			
				if (json.privacy=='public') {
					
					renderOtherCustom();
					getOtherFeed();
					
				} else {

					if (json.allowed) {

						renderOtherCustom();
						getOtherFeed();
						
					} else {
	
						alert("This user is private: send them a message if you want to get on their blog!");
						
					}
					
				}
				
			} else{

				alert(json.message);
			}
	}});	
	
});

function getOtherPage() {
	
	changeOther();
	
	$.ajax({type: 'POST', url: "MySQL/Privacy/getPrivacy.php", dataType: 'json', success: function(json) {
		if (json.success) {

			
				if (json.privacy=='public') {
					
					renderOtherCustom();
					getOtherFeed();
					
				} else {

					if (json.allowed) {

						renderOtherCustom();
						getOtherFeed();
						
					} else {
	
						alert("This user is private: send them a message if you want to get on their blog!");
						
					}
					
				}
				
			} else{

				alert(json.message);
			}
	}});	
}


function getOtherFeed() {
	$.ajax({url: "MySQL/Feed/otherFeed.php", success: function(result){
        $("#feed").html(result);
    }});
}

function changeOther() {
	var other = $("#otherUser").val();
	
	$.ajax({type: 'POST', url: "MySQL/Other/changeOther.php", data: {"other": other}, dataType: 'json', success: function(json) {
		if (json.success) {
		
		}else{	
			alert("couldn't");
		}
	}});	
}

function logOut() {
	
	$.ajax({url: 'MySQL/Custom/getCustom.php', dataType: 'json', success: function(json) {
	
	if (json.success) {
		
		alert("Bye!!");
		window.location.href = "OpeningPage.php"
		
	}else{
		
		alert("connection error");
	}
}});
}

function mainPage() {
	
	 window.location.href = "MainPage.php";
	 
}

</script>

<style>
    
html, body {
    height: 100%;
}

html {
    display: table;
    margin: auto;
}

body {
    font-family: "Lucida Console", Monaco, monospace;
    display: table-cell;
    vertical-align: middle;
}
	
h1 {
    font-size: 80px;
    font-weight: bold;
	max-width: 600px
}

input[type=button] {
    
	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 20px;
	background: #3498db;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
    
}

input[type=button]:hover {
  background: #ffd4ff;
  text-decoration: none;
  
}

input[type=button]:focus {
	outline:0;
}

input[type=submit] {
    
	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 20px;
	background: #3498db;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
    
}

input[type=submit]:hover {
	
  background: #ffd4ff;
  text-decoration: none;
  
}

input[type=submit]:focus {
	
	outline:0;
}

input[type=text] {
  font-family: "Lucida Console", Monaco, monospace;
}

input[type=file] {
 	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 15px;
	background: #00CC66;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
}

textarea {
	font-family: "Lucida Console", Monaco, monospace;
	line-height: 150%;
	border-width:0px;
	padding: 10px;
}

p {
	max-width: 550px;
	line-height: 150%
	
}

img {
	max-width: 600px;
}
hr { 
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-style: solid;
    border-width: 3px;
	color:#00CC66;
}

button {
    
	border-radius: 5px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #000000;
	font-size: 12px;
	background: #F6F6F6;
	padding: 7px 10px 5px 10px;
	text-decoration: none;
	border-width:0px
    
}

button:hover {
  background: #FF7575;
  text-decoration: none;
  
}

button:focus {
	outline:0;
}

</style>


</head>
<body>
		

<h1 align=center id=title> Share Space </h1>

<div id="header" align="center">

<input type="button" onclick= "searchForm()" value="Search">
&nbsp;&nbsp;
<input type="button" onclick= "mainPage()" value="Main">
&nbsp;&nbsp;
<input type="button" onclick= "logOut()" value="Log Out">
&nbsp;&nbsp;

</div>
<br>
<br>


<div id="feed" align="center">

</div>
<br>
<br>
<div id="redirect" align="center">

</div>
<br>
<br>
        
</body>
</html>