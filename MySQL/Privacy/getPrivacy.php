<?php

header("Content-Type: application/json"); 

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
    exit;
}

$stmt = $mysqli->prepare("select privacy FROM users WHERE username=?");
if(!$stmt){
    
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't retrieve theme!"
	));
    exit;
}

$username= $_SESSION["other"];

// Bind the parameter
$stmt->bind_param('s', $username);

$stmt->execute();

$result = $stmt->get_result();
 
while($row = $result->fetch_assoc()){
    
	$privacy = htmlentities($row["privacy"]);
}

$stmt->close();


if(!empty($privacy)){
    
    $stmt = $mysqli->prepare("select count(*) FROM privacy WHERE username=? and allows=?");
    if(!$stmt){
    
        echo json_encode(array(
            "success" => false,
            "message" => "Couldn't retrieve theme!"
            ));
            exit;
    }
    
    $username= $_SESSION["other"];
    $allows=$_SESSION["username"];
    
    $stmt->bind_param('ss', $username, $allows);
    
    $stmt->execute();
    
    $result2 = $stmt->get_result();
    
    while($row2 = $result2->fetch_assoc()){
    
        $count = htmlentities($row2["count(*)"]);
    }


    if ($count==0){
        
        echo json_encode(array(
            "success" => true,
            "privacy" => $privacy,
            "allowed" => false,
                
        ));
        exit;
        
    } else {
        
        echo json_encode(array(
            "success" => true,
            "privacy" => $privacy,
            "allowed" => true,
                
        ));
        exit;
        
    }
    
    $stmt2->close();

} else{
    
    echo json_encode(array(
        "success" => false,
        "message" => "not a user",
            
        ));
    exit;
}



    
?>
    