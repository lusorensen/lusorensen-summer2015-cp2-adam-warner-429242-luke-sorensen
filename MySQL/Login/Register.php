<?php
//takes in username and password and adds to user table

header("Content-Type: application/json");
ini_set("session.cookie_httponly", 1);
session_start();

if($_POST['password']!=$_POST['password2']){
    echo json_encode(array(
		"success" => false,
		"message" => "Passwords don't match"
	));
	exit;  
}

if(empty($_POST['newusername'])){
    echo json_encode(array(
		"success" => false,
		"message" => "Please enter new username"
	));
	exit; 
}

if( !preg_match('/^[\w_\-]+$/', $_POST['newusername']) ){
   echo json_encode(array(
		"success" => false,
		"message" => "Not a valid username format"
	));
	exit; 
}

if(empty($_POST['password'])){
    echo json_encode(array(
		"success" => false,
		"message" => "Please enter passwords"
	));
	exit;
}

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit; 
}

$username = htmlentities($_POST['newusername']);
$password = crypt(htmlentities($_POST['password']));

$stmt1 = $mysqli->prepare("SELECT COUNT(*) FROM users WHERE username=?");

// Bind the parameter
$stmt1->bind_param('s', $username);
$stmt1->execute();

$result = $stmt1->get_result();

while($row = $result->fetch_assoc()){
	$count= htmlentities($row["COUNT(*)"]);	
}

$stmt1->close();

if($count==0){

	$stmt = $mysqli->prepare("insert into users (username, password) values (?, ?)");
	if(!$stmt){
		echo json_encode(array(
			"success" => false,
			"message" => "Couldn't connect to database"
		));
		exit;
	}
	 
	$stmt->bind_param('ss', $username, $password);
	 
	$stmt->execute();
	
	$stmt->close();
	
	$stmt2 = $mysqli->prepare("insert into custom (username) values (?)");
	
	if(!$stmt2){
		echo json_encode(array(
			"success" => false,
			"message" => "Couldn't connect to database"
		));
		exit;
	}
	 
	$stmt2->bind_param('s', $username);
	 
	$stmt2->execute();
	 
	$stmt2->close();
	
	
	$_SESSION['username'] = $username;
	$_SESSION['token'] = substr(md5(rand()), 0, 10);
	
    $full_path = sprintf("/home/lsorensen/public_html/Final/Users/%s", $username);
    mkdir($full_path, 0777, true);
	 
	echo json_encode(array(
	"success" => true, 
	"message" => "New user $username registered!!"
	));
	exit;
	}

else{
	
	echo json_encode(array(
		"success" => false,
		"message" => "Username $username already taken"
	));
	exit; 
}
?>