<?php

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo ("helo");
}

$stmt = $mysqli->prepare("select receiver, time_sent, message FROM messages WHERE sender=? order by time_sent desc");

if(!$stmt){
	echo("error");
}

// Bind the parameter
$stmt->bind_param('s', $username);
$username= $_SESSION['username'];
$stmt->execute();

$result = $stmt->get_result();

while($row = $result->fetch_assoc()){
    
	$receiver = htmlentities($row["receiver"]);
	$time_sent = htmlentities($row["time_sent"]);
	$message = htmlentities($row["message"]);
    
        
        echo "<br>
        <h3>You sent to $receiver:</h3>
        <p> $message </p>
        <p> At: $time_sent</p>
        <hr>";
    
}

$stmt->close();

?>