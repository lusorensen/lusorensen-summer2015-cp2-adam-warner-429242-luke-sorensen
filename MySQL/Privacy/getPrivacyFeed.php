<?php

header("Content-Type: application/json"); 

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
    exit;
}

$stmt = $mysqli->prepare("select allows FROM privacy WHERE username=?");
if(!$stmt){
    
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't retrieve theme!"
	));
    exit;
}

$username= $_SESSION["username"];

// Bind the parameter
$stmt->bind_param('s', $username);

$stmt->execute();

$result = $stmt->get_result();
 
while($row = $result->fetch_assoc()){
    
	$privacy = htmlentities($row["allows"]);
    
    echo "<br><p>$privacy</p>";
}
$stmt->close();

?>
