<!DOCTYPE html>
<head>
<title>User Page</title>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="script/MainPage/headerForms.js"></script>
<script type="text/javascript" src="script/getComments.js"></script>

<script>
	
$(document).ready(function(){
	getPublicFeed();
});


function getPublicFeed() {
	$.ajax({type: 'POST', url: "MySQL/Feed/publicFeed.php", data: {"last": "1000"}, success: function(result){
        $("#feed").html(result);
    }});
}

function getMore(num) {
	$.ajax({type: 'POST', url: "MySQL/Feed/publicFeed.php", data: {"last": num}, success: function(result){
        $("#more").remove(); 
        $("#feed").append(result);
    }});
}


function logOut() {
	
	$.ajax({url: 'MySQL/Custom/getCustom.php', dataType: 'json', success: function(json) {
	
	if (json.success) {
		
		alert("Bye!!");
		window.location.href = "OpeningPage.php"
		
	}else{
		
		alert("connection error");
	}
}});
	
}

function getUserPage() {
	
	changeOther();
	window.location.href = "OtherUserPage.php"
	
}

function changeOther() {
	var other = $("#otherUser").val();
	
	$.ajax({type: 'POST', url: "MySQL/Other/changeOther.php", data: {"other": other}, dataType: 'json', success: function(json) {
		if (json.success) {
		
		}else{	
			alert("couldn't");
		}
	}});	
}

function userPage() {
    
    window.location.href = "UserPage.php"
    
}

</script>

<style>
	
html, body {
    height: 100%;
}

html {
    display: table;
    margin: auto;
}

body {
    font-family: "Lucida Console", Monaco, monospace;
    display: table-cell;
    vertical-align: middle;
    background-color: #B4C9ED;
}
	
h1 {
    font-size: 80px;
    font-weight: bold;
	max-width: 600px
}

input[type=button] {
    
	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 20px;
	background: #3498db;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
    
}

input[type=button]:hover {
  background: #ffd4ff;
  text-decoration: none;
  
}

input[type=button]:focus {
	outline:0;
}

input[type=submit] {
    
	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 20px;
	background: #3498db;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
    
}

input[type=submit]:hover {
	
  background: #ffd4ff;
  text-decoration: none;
  
}

input[type=submit]:focus {
	
	outline:0;
}

input[type=text] {
  font-family: "Lucida Console", Monaco, monospace;
}

input[type=file] {
 	border-radius: 20px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #ffffff;
	font-size: 15px;
	background: #00CC66;
	padding: 10px 15px 10px 15px;
	text-decoration: none;
	border-width:0px
}

textarea {
	font-family: "Lucida Console", Monaco, monospace;
	line-height: 150%;
	border-width:0px;
	padding: 10px;
}

p {
	max-width: 550px;
	line-height: 150%
	
}

img {
	max-width: 600px;
}
hr { 
    display: block;
    margin-left: auto;
    margin-right: auto;
    border-style: solid;
    border-width: 3px;
	color:#00CC66;
}

button {
    
	border-radius: 5px;
	font-family: "Lucida Console", Monaco, monospace;
	color: #000000;
	font-size: 12px;
	background: #F6F6F6;
	padding: 7px 10px 5px 10px;
	text-decoration: none;
	border-width:0px
    
}

button:hover {
  background: #FF7575;
  text-decoration: none;
  
}

button:focus {
	outline:0;
}


</style>

</head>
<body>

<h1 align=center id=title> Share Space </h1>

<div id="header" align="center">
<input type="button" onclick= "searchForm()" value="Search">
&nbsp;
<input type="button" onclick= "userPage()" value="Your Page">
&nbsp;
<input type="button" onclick= "logOut()" value="Log Out">
&nbsp;

</div>

<br>
<br>


<div id="feed" align="center">

</div>
<br>
<br>

<br>
<br>
        
</body>
</html>