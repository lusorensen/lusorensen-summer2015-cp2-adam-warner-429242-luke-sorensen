function getComments(id){
	
	$.ajax({type: 'POST', url: "MySQL/Comments/getComments.php", data: {"id": id}, success: function(result){
        $("#"+id).html(result); 
    }});

}

function retractComments(id){
	var result= "<input class=\"comments\" type=\"button\" onclick= \"getComments("+ id +")\" value=\"Comments\"><br>"
	$("#"+id).html(result); 
	
}

function addComment(id) {
	
	var comment = $("#commentText").val();
	
	$.ajax({type: 'POST', url: 'MySQL/Comments/createComment.php', data: {"id": id, "comment": comment},
	dataType: 'json', success: function(json) {
		if (json.success) {
			
			getComments(id);
		}else{	
			alert(json.message);
		}
	}});
	
	
}

function deleteComment(id, postId) {
	
	$.ajax({type: 'POST', url: 'MySQL/Comments/deleteComment.php', data: {"id": id},
	dataType: 'json', success: function(json) {
		if (json.success) {
			
			getComments(postId);
			
		}else{
			
			alert(json.message);
			
		}
	}});
	
	
}