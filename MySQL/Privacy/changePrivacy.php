<?php
//takes in username and password and adds to user table

header("Content-Type: application/json");
ini_set("session.cookie_httponly", 1);
session_start();


$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit; 
}
$stmt = $mysqli->prepare("update users set privacy= ? where username= ?");
    
if(!$stmt){
    echo json_encode(array(
        "success" => false,
        "message" => "Couldn't connect to database"
    ));
    exit;
}
$username=$_SESSION['username'];
$privacy=$_POST['privacy'];

$stmt->bind_param('ss', $privacy, $username);

$stmt->execute();

$stmt->close();

    echo json_encode(array(
    "success" => true, 
    "message" => "Privacy changed!!"
    ));
    exit;

?>
