<?php
// CalendarLogin.php

header("Content-Type: application/json"); // Since we are sending a JSON response here (not an HTML document), set the MIME Type to application/json

ini_set("session.cookie_httponly", 1);
session_start();

if(empty($_POST['username'])){
    echo json_encode(array(
		"success" => false,
		"message" => "Please enter username"
	));
	exit; 
}

if(empty($_POST['password'])){
    echo json_encode(array(
		"success" => false,
		"message" => "Please enter passwords"
	));
	exit;
}

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo json_encode(array(
		"success" => false,
		"message" => "Couldn't connect to database"
	));
	exit;
}

$stmt = $mysqli->prepare("SELECT COUNT(*), username, password FROM users WHERE username=?");

// Bind the parameter
$stmt->bind_param('s', $username);
$username = htmlentities($_POST['username']);
$stmt->execute();
 
// Bind the results
$stmt->bind_result($cnt, $user_id, $pwd_hash);
$stmt->fetch();
 
$pwd_guess = htmlentities($_POST['password']);

// Compare the submitted password to the actual password hash
if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){

	$_SESSION['username'] = $username;
	$_SESSION['token'] = substr(md5(rand()), 0, 10);
 
	echo json_encode(array(
		"success" => true,
		"message" => "Welcome $username!"
	));
	exit;
    
}else{
    echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Password or Username!"
	));
	exit;
    
}

?>