<?php

ini_set("session.cookie_httponly", 1);
session_start();

$mysqli = new mysqli('localhost', 'blog', 'blog', 'blog');
 
if($mysqli->connect_errno) {
	echo ("connection error");
}

$stmt = $mysqli->prepare("select sender, time_sent, message FROM messages WHERE receiver=? order by time_sent desc");

if(!$stmt){
	echo("error");
}

// Bind the parameter
$stmt->bind_param('s', $username);
$username= $_SESSION['username'];
$stmt->execute();

$result = $stmt->get_result();

while($row = $result->fetch_assoc()){
    
	$sender = htmlentities($row["sender"]);
	$time_sent = htmlentities($row["time_sent"]);
	$message = htmlentities($row["message"]);
    
        
        echo "<br>
        <h3> $sender said:</h3>
        <p> $message </p>
        <p> At: $time_sent</p>
        <hr>";
    
}

$stmt->close();

?>